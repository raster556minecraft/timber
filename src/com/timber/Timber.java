package com.timber;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Timber extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getLogger().info("Timber is now enabled.");
    }

    @Override
    public void onDisable() {
        getLogger().info("Timber is now disabled.");
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        // Make sure the player is in survival and they are breaking a log
        if (e.getPlayer().getGameMode().equals(GameMode.SURVIVAL) && e.getBlock().getBlockData().getMaterial().toString().toLowerCase().contains("log")) {

            ItemStack playerHand = e.getPlayer().getInventory().getItemInMainHand();
            // Only run if player is holding an axe
            if (playerHand.getType().toString().toLowerCase().contains("axe")) {
                // Get the damageable from the item meta
                Damageable damageable = (Damageable) playerHand.getItemMeta();
                assert damageable != null;
                // If the item is not near broken, use the ability
                if (damageable.getDamage() < playerHand.getType().getMaxDurability() - 1) {
                    e.setCancelled(true);
                    breakTree(e.getBlock(), 0, 0, 0, damageable, playerHand);
                }
            }
        }
    }

    // Break the tree recursively.
    private void breakTree(Block block, int branchX, int branchY, int branchZ, Damageable damageable, ItemStack stack) {
        if (Math.abs(branchX) < 4 && Math.abs(branchY) < 24 && Math.abs(branchZ) < 4) {
            // only break if this is a log
            if (block.getBlockData().getMaterial().toString().toLowerCase().contains("log")) {
                // If there is enough durability, break the block and set the damage to the axe
                if (damageable.getDamage() < stack.getType().getMaxDurability() - 1) {
                    block.breakNaturally();
                    damageable.setDamage(damageable.getDamage() + 1);
                    stack.setItemMeta((ItemMeta) damageable);
                } else {
                    return;
                }
                // run this method on new blocks in a 3x3x3 volume
                for (int x = -1; x < 2; x++) {
                    for (int y = -1; y < 2; y++) {
                        for (int z = -1; z < 2; z++) {
                            if (!(x == 0 && y == 0  && z == 0)) {
                                int finalX = x;
                                int finalY = y;
                                int finalZ = z;
                                Bukkit.getScheduler().runTaskLater(this, () -> breakTree(block.getRelative(finalX, finalY, finalZ), branchX + finalX, branchY + finalY, branchZ + finalZ, damageable, stack), 2);
                            }
                        }
                    }
                }
            }
        }
    }
}
